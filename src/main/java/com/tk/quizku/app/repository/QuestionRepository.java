package com.tk.quizku.app.repository;

import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.QuestionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, QuestionId> {
    Question findByQuestionIdQuizIdAndQuestionIdNumber(int quizId, int number);
}
