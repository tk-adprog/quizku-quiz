package com.tk.quizku.app.repository;

import com.tk.quizku.app.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, String>{
    Quiz findByIdQuiz(int idQuiz);

    List<Quiz> findByTitle(String title);

    List<Quiz> findByTagList(String tag);

    List<Quiz> findByQuizOwner(String username);
}
