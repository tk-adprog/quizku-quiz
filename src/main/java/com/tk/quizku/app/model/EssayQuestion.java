package com.tk.quizku.app.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class EssayQuestion extends Question {

    public EssayQuestion(String question, String answer) {
        super(question, answer, null);
    }
}