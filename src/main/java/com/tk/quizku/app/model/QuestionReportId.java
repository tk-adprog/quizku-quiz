package com.tk.quizku.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class QuestionReportId implements Serializable {
    private QuizReportId quizReportId;
    private int number;

    public QuestionReportId(QuizReportId quizReportId, int number) {
        this.quizReportId = quizReportId;
        this.number = number;
    }
}