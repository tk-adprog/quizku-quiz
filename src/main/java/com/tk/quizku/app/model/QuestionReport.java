package com.tk.quizku.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "question_report")
@Data
@NoArgsConstructor
public class QuestionReport {

    @EmbeddedId
    private QuestionReportId questionReportId;

    @JsonIgnore
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "quiz_report_quizId", referencedColumnName = "quizId",
                    updatable = false, nullable = false),
            @JoinColumn(name = "quiz_report_owner", referencedColumnName = "owner",
                    updatable = false, nullable = false)
    })
    private QuizReport questionReportOwner;

    @Column(name = "questionString")
    private String questionString;

    @Column(name = "rightAnswer")
    private String rightAnswer;

    @Column(name = "userAnswer")
    private String userAnswer;

    @Column(name = "isTrue")
    private boolean isTrue;

    public QuestionReport(
            String questionString, String rightAnswer, String userAnswer, boolean isTrue) {
        this.questionString = questionString;
        this.rightAnswer = rightAnswer;
        this.userAnswer = userAnswer;
        this.isTrue = isTrue;
    }

    @Override
    public String toString() {
        return "QuestionReport{"
                + "questionReportId=" + questionReportId
                + ", questionString='" + questionString + '\''
                + ", rightAnswer='" + rightAnswer + '\''
                + ", userAnswer='" + userAnswer + '\''
                + ", isTrue=" + isTrue
                + '}';
    }
}
