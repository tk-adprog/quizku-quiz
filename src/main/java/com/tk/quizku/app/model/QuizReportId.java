package com.tk.quizku.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class QuizReportId implements Serializable {
    private int quizId;
    private String owner;

    public QuizReportId(int quizId, String owner) {
        this.quizId = quizId;
        this.owner = owner;
    }
}
