package com.tk.quizku.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class QuestionId implements Serializable {
    private int quizId;
    private int number;

    public QuestionId(int quizId, int number) {
        this.quizId = quizId;
        this.number = number;
    }
}