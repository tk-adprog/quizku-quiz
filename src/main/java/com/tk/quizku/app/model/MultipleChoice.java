package com.tk.quizku.app.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.List;


@Entity
@NoArgsConstructor
public class MultipleChoice extends Question {

    public MultipleChoice(String question, List<String> option) {
        super(question, option.get(0), option);

    }
}