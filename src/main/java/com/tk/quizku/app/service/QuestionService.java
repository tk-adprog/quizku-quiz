package com.tk.quizku.app.service;

import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface QuestionService {

    void processListQuestionJson(List<HashMap<String, Object>> json, Quiz quiz);

    Iterable<Question> getListQuestion();

    Question getQuestion(int idQuiz, int number);

    Question processQuestionJson(Map<String, Object> json);

    Question processQuestionJson(String type, String question, List<String> options);

    void setQuestionId(Quiz quiz, Question question, int number);
}
