package com.tk.quizku.app.service;

import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    QuizRepository quizRepository;

    @Override
    public List<Quiz> getListQuizByTitle(String title) {
        return quizRepository.findByTitle(title);
    }

    public List<Quiz> getListQuizByTag(String tag) {
        return quizRepository.findByTagList(tag);
    }

//    public List<Quiz> getListQuizByTitle(String title) {
//        Set<String> setsub = new HashSet<>();
//        setsub.add(title);
//        return quizRepository.findQuizByTitle(setsub);
//    }
}
