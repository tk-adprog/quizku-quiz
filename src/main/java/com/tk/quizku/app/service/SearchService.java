package com.tk.quizku.app.service;

import com.tk.quizku.app.model.Quiz;

import java.util.List;

public interface SearchService {
//    Quiz getQuizByTitle(String title);

    Iterable<Quiz> getListQuizByTag(String tag);

    List<Quiz> getListQuizByTitle(String title);
}
