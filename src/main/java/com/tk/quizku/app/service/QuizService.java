package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.Quiz;

import java.util.List;
import java.util.Map;

public interface QuizService {

    Quiz processJson(Map<String, Object> json) throws JsonProcessingException;

    Quiz processQuizJson(Map<String, Object> json) throws JsonProcessingException;

    Quiz createQuiz(String title, String imageUrl, int duration,
                    List<String> listTag, String desc, String user);

    Iterable<Quiz> getListQuiz();

    Quiz getQuizByIdQuiz(int idQuiz);

    Iterable<Quiz> getQuizByOwner(Map<String, Object> json);

    boolean delQuiz(int idQuiz);

    void deleteAll();
}
