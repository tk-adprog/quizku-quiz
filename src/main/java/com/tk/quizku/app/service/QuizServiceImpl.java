package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.repository.QuestionRepository;
import com.tk.quizku.app.repository.QuizRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    QuizRepository quizRepository;
    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    QuestionService questionService;
    @Autowired
    RestService restService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;


    @Override
    public Quiz processJson(Map<String, Object> json) throws JsonProcessingException {
        Quiz quiz = processQuizJson(json);
        List<HashMap<String, Object>> questionJson =
                (List<HashMap<String, Object>>) json.get("questions");
        questionService.processListQuestionJson(questionJson, quiz);
        return quiz;
    }

    @Override
    public Quiz processQuizJson(Map<String, Object> json) throws JsonProcessingException {
        String title = json.get("title").toString();
        String imageUrl = json.get("imageUrl").toString();
        int duration = Integer.parseInt(json.get("duration").toString());
        String desc = json.get("description").toString();
        String tag = json.get("tag").toString();
        String[] tags = tag.split(" ");
        List<String> listTag = Arrays.asList(tags);
        String token = json.get("token").toString();
        String user = jwtTokenUtil.getUsernameFromToken(token);
        return createQuiz(title, imageUrl, duration, listTag, desc, user);
    }

    @Override
    public Quiz createQuiz(String title, String imageUrl, int duration,
                           List<String> listTag, String desc, String user) {

        Quiz quiz = new Quiz(title, imageUrl, duration, listTag, desc, user);
        quizRepository.save(quiz);
        return quiz;
    }

    @Override
    public Iterable<Quiz> getListQuiz() {
        return quizRepository.findAll();
    }

    @Override
    public Quiz getQuizByIdQuiz(int idQuiz) {
        return quizRepository.findByIdQuiz(idQuiz);
    }

    @Override
    public Iterable<Quiz> getQuizByOwner(Map<String, Object> json) {
        String token = json.get("token").toString();
        String user = jwtTokenUtil.getUsernameFromToken(token);
        return quizRepository.findByQuizOwner(user);
    }

    @Override
    public boolean delQuiz(int idQuiz) {
        Quiz quiz = quizRepository.findByIdQuiz(idQuiz);
        if (quiz == null) {
            return false;
        }
        quizRepository.delete(quiz);
        return true;
    }

    @Override
    public void deleteAll(){
        quizRepository.deleteAll();
    }
}
