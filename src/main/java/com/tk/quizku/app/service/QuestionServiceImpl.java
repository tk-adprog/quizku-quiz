package com.tk.quizku.app.service;

import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.QuestionId;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.repository.QuestionRepository;
import com.tk.quizku.app.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    QuizRepository quizRepository;


    @Override
    public void processListQuestionJson(List<HashMap<String, Object>> json, Quiz quiz) {
        for (int i = 0; i < json.size(); i++) {
            HashMap<String, Object> questionJson = json.get(i);
            Question question = processQuestionJson(questionJson);
            setQuestionId(quiz, question, i + 1);
        }
    }

    @Override
    public Iterable<Question> getListQuestion() {
        return questionRepository.findAll();
    }

    @Override
    public Question getQuestion(int idQuiz, int number) {
        return questionRepository.findByQuestionIdQuizIdAndQuestionIdNumber(idQuiz, number);
    }

    @Override
    public Question processQuestionJson(Map<String, Object> json) {
        String type = json.get("type").toString();
        String question = json.get("question").toString();
        ArrayList<Object> optionsObject = (ArrayList<Object>) json.get("options");
        List<String> optionsString = optionsObject.stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());
        return processQuestionJson(type, question, optionsString);
    }

    @Override
    public Question processQuestionJson(String type, String question, List<String> options) {
        QuestionFactory questionFactory = new QuestionFactory();
        return questionFactory.createQuestion(type, question, options);
    }

    @Override
    public void setQuestionId(Quiz quiz, Question question, int number) {
        QuestionId questionId = new QuestionId(quiz.getIdQuiz(), number);
        question.setQuestionId(questionId);
        question.setQuestionOwner(quiz);
        quiz.getQuestionList().add(question);
        questionRepository.save(question);
    }
}

