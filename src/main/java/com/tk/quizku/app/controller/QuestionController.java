package com.tk.quizku.app.controller;

import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping(path = "/{idQuiz}/{number}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getQuestion(@PathVariable(value = "idQuiz") int idQuiz,
                                      @PathVariable(value = "number") int number) {
        Question question = questionService.getQuestion(idQuiz, number);
        if (question == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(question);
    }

    @GetMapping(path = "", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Question>> getListQuestion() {
        return ResponseEntity.ok(questionService.getListQuestion());
    }


}

