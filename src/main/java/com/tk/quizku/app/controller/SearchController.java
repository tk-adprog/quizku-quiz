package com.tk.quizku.app.controller;


import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @GetMapping(path = "/tag/{tag}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Quiz>> getListQuiz(@PathVariable(value = "tag") String tag) {
        List<Quiz> listQuiz = (List<Quiz>) searchService.getListQuizByTag(tag);
        if (listQuiz.size() == 0) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(listQuiz);
    }

    @GetMapping(path = "/{title}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Quiz>> getListQuizSub(@PathVariable(value = "title") String title) {
        List<Quiz> listQuiz = searchService.getListQuizByTitle(title);
        if (listQuiz == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(listQuiz);
    }

}