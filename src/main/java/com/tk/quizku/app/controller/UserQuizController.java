package com.tk.quizku.app.controller;


import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.service.UserQuizService;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/user")
public class UserQuizController {

    @Autowired
    private UserQuizService userQuizService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @GetMapping(path = "/{username}", produces = {"application/json"})
    @CrossOrigin
    @ResponseBody
    public ResponseEntity<UserQuiz> getUser( @PathVariable(value ="username") String  username ) {
        UserQuiz user = userQuizService.getUserByUsername(username);
        return ResponseEntity.ok(user);
    }

    @GetMapping(path = "/all", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<UserQuiz>> getAllUser() {
        return ResponseEntity.ok(userQuizService.getAllUser());
    }

    @GetMapping(path = "/self/destruct", produces = {"application/json"})
    @CrossOrigin
    public String deleteAll() {
        userQuizService.deleteAll();
        return "redirect:/user/all";
    }

}