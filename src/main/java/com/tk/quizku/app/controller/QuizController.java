package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/quiz")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @PostMapping(path = "/register", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity createQuiz(@RequestBody Map<String, Object> json) throws JsonProcessingException {
        Quiz quiz = quizService.processJson(json);
        return ResponseEntity.ok(quiz);
    }

    @GetMapping(path = "/{idQuiz}", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getQuiz(@PathVariable(value = "idQuiz") int idQuiz) {
        Quiz quiz = quizService.getQuizByIdQuiz(idQuiz);
        if (quiz == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(quiz);
    }

    @GetMapping(path = "/all", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity<Iterable<Quiz>> getListQuiz() {
        return ResponseEntity.ok(quizService.getListQuiz());
    }

    @DeleteMapping(path = "/{idQuiz}", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity delQuiz(@PathVariable(value = "idQuiz") int idQuiz) {
        boolean isSuccess = quizService.delQuiz(idQuiz);
        if (!isSuccess) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/getQuiz", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getQuiz(@RequestBody Map<String, Object> json) {
        return ResponseEntity.ok(quizService.getQuizByOwner(json));
    }

    @GetMapping(path = "/self/destruct", produces = {"application/json"})
    @CrossOrigin
    public String deleteAll() {
        quizService.deleteAll();
        return "redirect:/quiz/all";
    }



}


