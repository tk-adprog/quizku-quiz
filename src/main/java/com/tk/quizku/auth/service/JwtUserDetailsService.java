package com.tk.quizku.auth.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.UserQuizRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserQuizRepository userQuizRepository;

    @Autowired
    RestService restService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    private PasswordEncoder bcryptEncoder;
    // mencari user dari user repository
    // lalu masukkan username dan password dari yang bersangkutan ke dalam user details
    // $2a$10$m0IlnWw8I5P39bYv4Gmdqu8L39WJJQvHnQeEmQTDAOmYK4R3Nkv.O

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserQuiz userku = userQuizRepository.findByUsername(username);
        if (userku == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(userku.getUsername(), bcryptEncoder.encode(userku.getPassword()),
                new ArrayList<>());
    }

//    @SneakyThrows
//    @Override
//    public UserDetails loadUserByUsername(String token) throws UsernameNotFoundException {
//
//        String username = jwtTokenUtil.getUsernameFromToken(token);
//        String userJson = restService.getPostsPlainjson(String.format("https://quiz-ku.herokuapp.com/user/%s", username), "token");
//        //String userJson = restService.getPostsPlainjson(String.format("http://localhost:8080/user/%s", username), token);
//        ObjectMapper objectMapper = new ObjectMapper();
//        UserQuiz userku = objectMapper.readValue(userJson, UserQuiz.class);
//        if (userku == null) {
//            throw new UsernameNotFoundException("User not found with username: " + username);
//        }
//        return new org.springframework.security.core.userdetails.User(userku.getUsername(), bcryptEncoder.encode(userku.getPassword()),
//                new ArrayList<>());
//    }

}
