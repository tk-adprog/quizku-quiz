package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.service.QuizServiceImpl;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = QuizController.class)
public class QuizControllerTest {

    private final Map<String, Object> json = new HashMap<>();
    @Autowired
    private MockMvc mvc;
    @MockBean
    private QuizServiceImpl quizService;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private Quiz quiz;
    private UserQuiz userQuiz;
    private String userQuizToken = "token";
    private UserDetails userQuizUserDetail;

    @BeforeEach
    public void setUp() {

        Date tanggal = (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );
        userQuizUserDetail = new User("1906192052", "passwordmeow123", new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);


        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        quiz = new Quiz("judul", "url", 30, tag, "ini quiz", userQuiz.toString());
        List<String> options = new ArrayList<>();
        options.add("a");
        options.add("b");
        options.add("c");
        options.add("d");
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questions = new ArrayList<>();
        questions.add(questionFactory.createQuestion("choice", "pg?", options));
        options.add("true");
        questions.add(questionFactory.createQuestion("essay", "essay?", options));

        json.put("title", "judul");
        json.put("imageUrl", "url");
        json.put("description", "ini quiz");
        json.put("duration", 30);
        json.put("tag", "tag1 tag2");
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("type", "choice");
        map1.put("question", "pg?");
        String[] option = new String[]{"a, b, c, d"};
        map1.put("options", option);
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("type", "essay");
        map2.put("question", "essay?");
        map2.put("options", option);
        HashMap<String, Object>[] questionsMap = new HashMap[2];
        questionsMap[0] = map1;
        questionsMap[1] = map2;
        json.put("questions", questionsMap);
        json.put("token", userQuizToken);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerCreateQuiz() throws Exception {
        when(quizService.processJson(json)).thenReturn(quiz);
        mvc.perform(post("/quiz/register").contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .content(mapToJson(quiz)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerGetQuiz() throws Exception {
        when(quizService.getQuizByIdQuiz(0)).thenReturn(quiz);
        mvc.perform(get("/quiz/0")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("judul"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerGetQuizByOwner() throws Exception {
        List<Quiz> listQuiz = new ArrayList<>();
        listQuiz.add(quiz);
        when(quizService.getQuizByOwner(any())).thenReturn(listQuiz);
        mvc.perform(post("/quiz/getQuiz")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(quiz)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("judul"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerGetQuizNotExist() throws Exception {
        when(quizService.getQuizByIdQuiz(0)).thenReturn(null);
        mvc.perform(get("/quiz/0").contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerGetListQuiz() throws Exception {
        List<Quiz> listQuiz = new ArrayList<>();
        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        Quiz quiz2 = new Quiz("judul2", "url", 40, tag, "desc", userQuiz.toString());
        listQuiz.add(quiz);
        listQuiz.add(quiz2);
        when(quizService.getListQuiz()).thenReturn(listQuiz);
        mvc.perform(get("/quiz/all")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("judul"))
                .andExpect(jsonPath("$[1].title").value("judul2"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerDeleteQuiz() throws Exception {
        when(quizService.delQuiz(0)).thenReturn(true);
        quizService.processJson(json);
        mvc.perform(delete("/quiz/0")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerDeleteQuizButNotFound() throws Exception {
        when(quizService.delQuiz(0)).thenReturn(false);
        mvc.perform(delete("/quiz/0")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken).header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerDeleteAll() throws Exception {
        mvc.perform(get("/quiz/self/destruct")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().is3xxRedirection());

    }

}



