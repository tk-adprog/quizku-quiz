package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.service.QuestionServiceImpl;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = QuestionController.class)
public class QuestionControllerTest {

    private final Map<String, Object> json = new HashMap<>();
    @Autowired
    private MockMvc mvc;
    @MockBean
    private QuestionServiceImpl questionService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;
    private Quiz quiz;


    @BeforeEach
    public void setUp() {

        Date tanggal = (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen", "password", new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        json.put("title", "judul");
        json.put("description", "Descripsi quiz");
        json.put("tag", "Tag1 Tag2 AdalahTag");
        json.put("duration", 30);
        json.put("imageUrl", "url");
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("type", "Choice");
        map1.put("question", "Pertanyaan Test");
        String[] tags1 = new String[]{"Test, Tast, Tist, Tust"};
        map1.put("options", tags1);
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("type", "Essay");
        map2.put("question", "Pertanyaan Test");
        String[] tags2 = new String[]{"Test"};
        map2.put("options", tags2);
        HashMap<String, Object>[] questionsMap = new HashMap[2];
        questionsMap[0] = map1;
        questionsMap[1] = map2;
        json.put("questions", questionsMap);

        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        quiz = new Quiz("judul", "url", 30, tag, "ini quiz", null);
        List<String> options = new ArrayList<>();
        options.add("a");
        options.add("b");
        options.add("c");
        options.add("d");
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questionsList = new ArrayList<>();
        questionsList.add(questionFactory.createQuestion("choice", "pg?", options));
        options.add("true");
        questionsList.add(questionFactory.createQuestion("essay", "essay?", options));
        quiz.setQuestionList(questionsList);

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuestionControllerGetQuestion() throws Exception {
        when(questionService.getQuestion(0, 1)).thenReturn(quiz.getQuestionList().get(0));
        mvc.perform(get("/question/0/1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(quiz)))
                .andExpect(jsonPath("$.question").value("pg?"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuestionControllerGetQuestionButNotFound() throws Exception {
        when(questionService.getQuestion(0, 1)).thenReturn(null);
        mvc.perform(get("/question/0/1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(quiz)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuestionControllerGetListQuestion() throws Exception {
        when(questionService.getListQuestion()).thenReturn(quiz.getQuestionList());
        mvc.perform(get("/question").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].question").value("pg?"))
                .andExpect(jsonPath("$[1].question").value("essay?"));
    }
}


