package com.tk.quizku.app.controller;

import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.service.SearchServiceImpl;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SearchController.class)
public class SearchControllerTest {

    private final Map<String, Object> json = new HashMap<>();
    @Autowired
    private MockMvc mvc;
    @MockBean
    private SearchServiceImpl searchService;
    @MockBean
    private JwtTokenUtil jwtTokenUtil;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;
    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private Quiz quizPertama;
    private Quiz quizKedua;
    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;
    private List<Quiz> quizList;


    @BeforeEach
    public void setUp() {
        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");

        // buat quiz dummy pertama
        quizPertama = new Quiz("judul", "url", 30, tag, "ini quiz pertama", null);
        List<String> optionsPertama = new ArrayList<>();
        optionsPertama.add("a");
        optionsPertama.add("b");
        optionsPertama.add("c");
        optionsPertama.add("d");

        // buat quiz dummy kedua
        quizKedua = new Quiz("judul", "url", 30, tag, "ini quiz Kedua", null);
        List<String> optionsKedua = new ArrayList<>();
        optionsKedua.add("a");
        optionsKedua.add("b");
        optionsKedua.add("c");
        optionsKedua.add("d");

        // masukkan ke dalam list
        quizList = new ArrayList<>();
        quizList.add(quizPertama);
        quizList.add(quizKedua);

        // buat question factory dummy
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questions = new ArrayList<>();
        questions.add(questionFactory.createQuestion("choice", "pg?", optionsPertama));
        optionsPertama.add("true");
        questions.add(questionFactory.createQuestion("essay", "essay?", optionsPertama));

        json.put("title", "judul");
        json.put("imageUrl", "url");
        json.put("description", "ini quiz Pertama");
        json.put("duration", 30);
        json.put("tag", "tag1 tag2");
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("type", "choice");
        map1.put("question", "pg?");
        String[] option = new String[]{"a, b, c, d"};
        map1.put("options", option);
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("type", "essay");
        map2.put("question", "essay?");
        map2.put("options", option);
        HashMap<String, Object>[] questionsMap = new HashMap[2];
        questionsMap[0] = map1;
        questionsMap[1] = map2;
        json.put("questions", questionsMap);

        // tambahan untuk jwt login

        Date tanggal = (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen", "password", new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

    }
//
//    // gunakan untuk mengubah suatu objek contoh quiz menjadi json
//    private String mapToJson(Object obj) throws JsonProcessingException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        return objectMapper.writeValueAsString(obj);
//    }



    @Test
    @WithMockUser(username = "ian.andersen")
    public void testTitleSearchExists() throws Exception {
        when(searchService.getListQuizByTitle("judul")).thenReturn(quizList);

        // profiling for(int i=0; i < 100000; i++ ){
        mvc.perform(get("/judul")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        //}

    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testTitleSearchNotExisting() throws Exception {
        when(searchService.getListQuizByTitle("ian")).thenReturn(null);
        mvc.perform(get("/ian")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testTagSearchNonExisting() throws Exception {
        List<Quiz> listQuiz = new ArrayList<>();
        when(searchService.getListQuizByTag("ian")).thenReturn(listQuiz);
        mvc.perform(get("/tag/ian")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testTagSearchExists() throws Exception {
        List<Quiz> listQuiz = new ArrayList<>();
        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        Quiz quiz2 = new Quiz("judul2", "url", 40, tag, "desc", null);
        listQuiz.add(quizPertama);
        listQuiz.add(quiz2);
        when(searchService.getListQuizByTag("tag1")).thenReturn(listQuiz);
        mvc.perform(get("/tag/tag1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("judul"))
                .andExpect(jsonPath("$[1].title").value("judul2"));
    }


}

