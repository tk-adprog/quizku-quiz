package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuestionRepository;
import com.tk.quizku.app.repository.QuizRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class QuizServiceImplTest {
    private final Map<String, Object> json = new HashMap<>();
    @Mock
    private QuizRepository quizRepository;
    @Mock
    private QuestionRepository questionRepository;
    @Mock
    private QuestionServiceImpl questionService;
    @Mock
    private JwtTokenUtil jwtTokenUtil;
    @Mock
    private RestService restService;
    @InjectMocks
    private QuizServiceImpl quizService;
    private Quiz quiz;
    private UserQuiz userQuiz;

    @BeforeEach
    public void setUp() {
        Date tanggal = (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        quiz = new Quiz("judul", "url", 30, tag, "ini quiz", userQuiz.getUsername());
        List<String> options = new ArrayList<>();
        options.add("a");
        options.add("b");
        options.add("c");
        options.add("d");
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questions = new ArrayList<>();
        questions.add(questionFactory.createQuestion("choice", "pg?", options));
        options.add("true");
        questions.add(questionFactory.createQuestion("essay", "essay?", options));

        json.put("title", "judul");
        json.put("imageUrl", "url");
        json.put("description", "ini quiz");
        json.put("duration", 30);
        json.put("tag", "tag1 tag2");
        HashMap<String, Object> q1 = new HashMap<>();
        q1.put("type", "choice");
        q1.put("question", "pg?");
        String[] option = new String[]{"a, b, c, d"};
        q1.put("options", option);
        HashMap<String, Object> q2 = new HashMap<>();
        q2.put("type", "essay");
        q2.put("question", "essay?");
        q2.put("options", option);
        List<HashMap<String, Object>> questionsMap = new ArrayList<>();
        questionsMap.add(q1);
        questionsMap.add(q2);
        json.put("questions", questionsMap);
        json.put("token", "token");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testQuizServiceProcessJson() throws JsonProcessingException {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(userQuiz));
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        Quiz quizResult = quizService.processJson(json);
        assertEquals(quiz.toString(), quizResult.toString());
    }

    @Test
    public void testQuizServiceProcessQuizJson() throws JsonProcessingException {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(userQuiz));
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        Quiz quizResult = quizService.processQuizJson(json);
        assertEquals(quiz.toString(), quizResult.toString());
    }

    @Test
    public void testQuizServiceQuizCreateQuiz() {
        quizService.createQuiz(quiz.getTitle(), quiz.getImageUrl(),
                quiz.getDuration(), quiz.getTagList(), quiz.getDescription(), userQuiz.getUsername());
        assertEquals(0, quiz.getIdQuiz());
    }

    @Test
    public void testQuizServiceQuizGetListquiz() {
        Iterable<Quiz> listquiz = quizRepository.findAll();
        lenient().when(quizService.getListQuiz()).thenReturn(listquiz);
        Iterable<Quiz> listquizResult = quizService.getListQuiz();
        assertIterableEquals(listquiz, listquizResult);
    }

    @Test
    public void testQuizServiceQuizgetQuizByIdQuiz() {
        lenient().when(quizService.getQuizByIdQuiz(0)).thenReturn(quiz);
        Quiz resultquiz = quizService.getQuizByIdQuiz(quiz.getIdQuiz());
        assertEquals(quiz.getIdQuiz(), resultquiz.getIdQuiz());
    }

    @Test
    public void testQuizServiceQuizgetQuizByOwner() {
        List<Quiz> listQuiz = new ArrayList<>();
        listQuiz.add(quiz);
        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn("user");
        lenient().when(quizRepository.findByQuizOwner(any())).thenReturn(listQuiz);
        Iterable<Quiz> results = quizService.getQuizByOwner(json);
        assertEquals(listQuiz, results);
    }

    @Test
    public void testQuizServiceQuizDeletequiz() {
        quizService.createQuiz(quiz.getTitle(), quiz.getImageUrl(),
                quiz.getDuration(), quiz.getTagList(), quiz.getDescription(), userQuiz.getUsername());
        when(quizRepository.findByIdQuiz(0)).thenReturn(quiz);
        quizService.delQuiz(0);
        lenient().when(quizService.getQuizByIdQuiz(0)).thenReturn(null);
        assertEquals(null, quizService.getQuizByIdQuiz(quiz.getIdQuiz()));
    }

    @Test
    void testQuizServiceQuizDeletequizButNotFound() {
        when(quizService.getQuizByIdQuiz(0)).thenReturn(null);
        quizService.delQuiz(quiz.getIdQuiz());
        assertEquals(null, quizService.getQuizByIdQuiz(quiz.getIdQuiz()));
    }

    @Test
    void testQuizServiceDeleteAllQuiz() {
        quizService.deleteAll();
    }

}


