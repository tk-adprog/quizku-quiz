package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.repository.QuestionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplTest {
    @Mock
    private final QuestionFactory questionFactory = new QuestionFactory();
    private final List<Question> questions = new ArrayList<>();
    private final List<HashMap<String, Object>> questionsMap = new ArrayList<>();
    @Mock
    private QuestionRepository questionRepository;
    @InjectMocks
    private QuestionServiceImpl questionService;

    @BeforeEach
    public void setUp() {
        List<String> options = new ArrayList<>();
        options.add("a");
        options.add("b");
        options.add("c");
        options.add("d");
        QuestionFactory questionFactory = new QuestionFactory();
        questions.add(questionFactory.createQuestion("choice", "pg?", options));
        options.add("true");
        questions.add(questionFactory.createQuestion("essay", "essay?", options));

        HashMap<String, Object> q1 = new HashMap<>();
        q1.put("type", "choice");
        q1.put("question", "pg?");
        q1.put("options", options);
        HashMap<String, Object> q2 = new HashMap<>();
        q2.put("type", "essay");
        q2.put("question", "essay?");
        q2.put("options", options);
        questionsMap.add(q1);
        questionsMap.add(q2);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testQuestionServiceProcessJson() {
        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        Quiz quiz = new Quiz("judul", "url", 30, tagList, "ini quiz", null);
        quiz.setIdQuiz(0);
        questionService.processListQuestionJson(questionsMap, quiz);
        verify(questionRepository, atLeastOnce()).save(any());
    }

    @Test
    public void testQuestionServiceCreateQuestionFromJson() {
        Question questionResult = questionService.processQuestionJson(questionsMap.get(0));
        assertEquals(questions.get(0).toString(), questionResult.toString());
    }


    @Test
    public void testQuestionServiceQuestionCreateQuestion() {
        lenient().when(questionFactory.createQuestion("choice", "pg?",
                questions.get(0).getOption())).thenReturn(questions.get(0));
        Question questionResult = questionService.processQuestionJson("choice",
                "pg?", questions.get(0).getOption());
        assertEquals(questions.get(0).toString(), questionResult.toString());
    }

    @Test
    public void testQuestionServiceQuestionGetListQuestion() {
        Iterable<Question> listQuestion = questionRepository.findAll();
        lenient().when(questionService.getListQuestion()).thenReturn(listQuestion);
        Iterable<Question> listQuestionResult = questionService.getListQuestion();
        assertIterableEquals(listQuestion, listQuestionResult);
    }

    @Test
    public void testQuestionServiceQuestiongetQuestionByQuestionIdAndNumber() {
        lenient().when(questionService.getQuestion(1, 1)).thenReturn(questions.get(1));
        Question resultQuestion = questionService.getQuestion(1, 1);
        assertEquals(questions.get(1).getQuestion(), resultQuestion.getQuestion());
    }

    @Test
    public void testQuestionServiceSetQuestionId() {
        List<String> tagList = new ArrayList<>();
        tagList.add("tag");
        Quiz quiz = new Quiz("judul", "url", 40, tagList, "ini quiz", null);
        quiz.setIdQuiz(1);
        questionService.setQuestionId(quiz, questions.get(1), 1);
        assertEquals(1, questions.get(1).getQuestionId().getNumber());
    }


}


