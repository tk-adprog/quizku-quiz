package com.tk.quizku.app.service;

import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Question;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuestionRepository;
import com.tk.quizku.app.repository.QuizRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class SearchServiceImplTest {

    @Autowired
    private MockMvc mvc;
    @InjectMocks
    private SearchServiceImpl searchService;
    @Mock
    private QuizRepository quizRepository;
    @Mock
    private QuestionRepository questionRepository;
    @Mock
    private QuestionServiceImpl questionService;
    @Mock
    private JwtTokenUtil jwtTokenUtil;
    @Mock
    private RestService restService;



    private final Map<String, Object> json = new HashMap<>();
    private Quiz quizPertama;
    private Quiz quizKedua;
    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;
    private List<Quiz> quizList;
    private Set<String> setBenar;


    @BeforeEach
    public void setUp() {
        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");

        // buat quiz dummy pertama
        quizPertama = new Quiz("judul", "url", 30, tag, "ini quiz pertama", null);
        List<String> optionsPertama = new ArrayList<>();
        optionsPertama.add("a");
        optionsPertama.add("b");
        optionsPertama.add("c");
        optionsPertama.add("d");

        // buat quiz dummy kedua
        quizKedua = new Quiz("judul", "url", 30, tag, "ini quiz Kedua", null);
        List<String> optionsKedua = new ArrayList<>();
        optionsKedua.add("a");
        optionsKedua.add("b");
        optionsKedua.add("c");
        optionsKedua.add("d");

        setBenar = new HashSet<>();
        setBenar.add("judul");

        // masukkan ke dalam list
        quizList = new ArrayList<>();
        quizList.add(quizPertama);
        quizList.add(quizKedua);

        // buat question factory dummy
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questions = new ArrayList<>();
        questions.add(questionFactory.createQuestion("choice", "pg?", optionsPertama));
        optionsPertama.add("true");
        questions.add(questionFactory.createQuestion("essay", "essay?", optionsPertama));

        json.put("title", "judul");
        json.put("imageUrl", "url");
        json.put("description", "ini quiz Pertama");
        json.put("duration", 30);
        json.put("tag", "tag1 tag2");
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("type", "choice");
        map1.put("question", "pg?");
        String[] option = new String[]{"a, b, c, d"};
        map1.put("options", option);
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("type", "essay");
        map2.put("question", "essay?");
        map2.put("options", option);
        HashMap<String, Object>[] questionsMap = new HashMap[2];
        questionsMap[0] = map1;
        questionsMap[1] = map2;
        json.put("questions", questionsMap);

        // tambahan untuk jwt login

        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz( "ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

    }

//    @Test
//    @WithMockUser(username = "ian.andersen")
//    public void testSearchServiceGetQuizByTitle() {
//        when(quizRepository.findQuizByTitle(setBenar)).thenReturn(quizList);
//        System.out.println(quizList.get(0));
//        List<Quiz> resultquiz = searchService.getListQuizByTitle(quizList.get(0).getTitle());
//        assertEquals(quizList.get(0).getTitle(), resultquiz.get(0).getTitle());
//    }

    @Test
    public void testSearchServiceGetQuizByTag() throws Exception {
        lenient().when(quizRepository.findByTagList("tag1")).thenReturn(quizList);
        List<Quiz> resultquiz = searchService.getListQuizByTag(quizList.get(0).getTagList().get(0));
        assertIterableEquals(quizList, resultquiz);
    }

    @Test
    public void testSearchServiceGetQuizByTitle() throws Exception {
        lenient().when(quizRepository.findByTitle("tag1")).thenReturn(quizList);
        List<Quiz> resultquiz = searchService.getListQuizByTitle(quizList.get(0).getTagList().get(0));
        assertIterableEquals(quizList, resultquiz);
    }

}



